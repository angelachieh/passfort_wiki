PassFort Wiki
=============


Install
-------

After cloning the repository, enter it:

    # clone the repository
    git clone passfort.bundle passfort_wiki
    cd passfort_wiki

Create a virtualenv and activate it:

    virtualenv -p python3 venv
    source venv/bin/activate

Install required Python packages:

    pip install -r requirements.txt


Run
---

On the command line, run:

    export FLASK_APP=wiki
    export FLASK_ENV=development
    flask init-db
    flask run

Open http://127.0.0.1:5000 in a browser.

When deploying, default configuration should be overridden with values taken from a configuration file. Create a `config.cfg` file in the `instance` folder, with a strong secret key and the database path. Example:

    SECRET_KEY='HDEyukBE9WmxefDrxh',
    DATABASE='/tmp/data.db'


Test
----

Run tests:

    pytest


References
----------

This code was based on https://github.com/pallets/flask/tree/master/examples/tutorial.