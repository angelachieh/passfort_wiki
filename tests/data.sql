INSERT INTO document (title)
VALUES
  ('Title1'),
  ('This is a title');

INSERT INTO revision (document_id, content, created)
VALUES
  (1, 'Hello world.', '2018-01-01 12:00:00'),
  (1, 'Once upon a time...', '2018-06-01 12:00:00'),
  (2, 'Lorem ipsum dolor sit amet.', '2018-05-01 12:00:00'),
  (2, 'Consectetur adipiscing elit.', '2018-06-01 12:00:00');
