import pytest
from wiki.db import get_db
import json

def test_list_documents(client):
    res = client.get('/documents')
    assert res.status_code == 200
    data = json.loads(res.data)
    assert len(data) == 2
    assert 'Title1' in data
    assert 'This is a title' in data 

def test_list_revisions(client):
    res = client.get('/documents/Title1')
    assert res.status_code == 200
    data = json.loads(res.data)
    data = [el['content'] for el in data]
    assert len(data) == 2
    assert 'Hello world.' in data
    assert 'Once upon a time...' in data

    res = client.get('/documents/This is a title')
    assert res.status_code == 200
    data = json.loads(res.data)
    data = [el['content'] for el in data]
    assert len(data) == 2
    assert 'Lorem ipsum dolor sit amet.' in data
    assert 'Consectetur adipiscing elit.' in data

def test_revision_at_timestamp(client):
    res = client.get('/documents/Title1/2018-06-01')
    assert res.status_code == 200
    data = json.loads(res.data)
    assert data == 'Hello world.'

    res = client.get('/documents/Title1/2018-07-01')
    assert res.status_code == 200
    data = json.loads(res.data)
    assert data == 'Once upon a time...'

    res = client.get('/documents/This is a title/2018-06-01')
    assert res.status_code == 200
    data = json.loads(res.data)
    assert data == 'Lorem ipsum dolor sit amet.'

    res = client.get('/documents/This is a title/2018-07-01')
    assert res.status_code == 200
    data = json.loads(res.data)
    assert data == 'Consectetur adipiscing elit.'

def test_latest_revision(client):
    res = client.get('/documents/Title1/latest')
    assert res.status_code == 200
    data = json.loads(res.data)
    assert data == 'Once upon a time...'

    res = client.get('/documents/This is a title/latest')
    assert res.status_code == 200
    data = json.loads(res.data)
    assert data == 'Consectetur adipiscing elit.'

def test_create_revision(client, app):
    res = client.post('/documents/Title1',
                      data=json.dumps({'content': 'lalala'}),
                      content_type='application/json')
    assert res.status_code == 200
    res = client.get('/documents/Title1/latest')
    data = json.loads(res.data)
    assert data == 'lalala'

    res = client.post('/documents/This is a title',
                      data=json.dumps({'content': 'blabla'}),
                      content_type='application/json')
    assert res.status_code == 200
    res = client.get('/documents/This is a title/latest')
    data = json.loads(res.data)
    assert data == 'blabla'
