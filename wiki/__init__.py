from flask import Flask, request, jsonify
import os
from wiki.db import get_db, init_app

class APIError(Exception):
    def __init__(self, message, status_code=400):
        super(APIError, self).__init__(message)
        self.message = message
        self.status_code = status_code

def create_app(test_config=None):

    # Create and configure an instance of the Flask application
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY='secret',
        # store the database in the instance folder
        DATABASE=os.path.join(app.instance_path, 'wiki.sqlite')
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.cfg', silent=True)
    else:
        # load the test config if passed in
        app.config.update(test_config)

    # Ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Register the database commands
    init_app(app)

    # Error handlers
    # --------------

    @app.errorhandler(404)
    def not_found(e):
        return jsonify({'message': 'Not found'}), 404

    @app.errorhandler(APIError)
    def handle_api_exception(error):
        return jsonify({'message': error.message}), error.status_code

    # Routes
    # ------

    @app.route('/documents', methods=['GET'])
    def list_titles():
        # List all available titles (most recent first)
        db = get_db()
        res = db.execute("""
            SELECT title
            FROM document
            ORDER BY created DESC
        """).fetchall()
        titles = [r[0] for r in res]
        return jsonify(titles)

    @app.route('/documents/<title>', methods=['GET'])
    def list_revisions(title):
        # List available revisions for a document (most recent first)
        db = get_db()
        res = db.execute("""
            SELECT revision.created,
                revision.content
            FROM document, revision
            WHERE document.id = revision.document_id
            AND document.title = '%s'
            ORDER BY revision.created DESC
        """ % title).fetchall()
        revisions = [{'timestamp': r[0], 'content': r[1]} for r in res]
        return jsonify(revisions)

    @app.route('/documents/<title>/<timestamp>', methods=['GET'])
    def get_revision(title, timestamp):
        # Get the content of a document as it was at a specific timestamp
        db = get_db()
        res = db.execute("""
            SELECT revision.content
            FROM document, revision
            WHERE document.id = revision.document_id
            AND document.title = '%s'
            AND revision.created <= '%s'
            ORDER BY revision.created DESC
            LIMIT 1
        """ % (title, timestamp)).fetchall()
        if len(res) < 1:
            raise APIError('Not Found', 404)
        revision = res[0][0]
        return jsonify(revision)

    @app.route('/documents/<title>/latest ', methods=['GET'])
    def get_latest_revision(title):
        # Get the content of the latest revision of a document
        db = get_db()
        res = db.execute("""
            SELECT revision.content
            FROM document, revision
            WHERE document.id = revision.document_id
            AND document.title = '%s'
            ORDER BY revision.created DESC
            LIMIT 1
        """ % title).fetchall()
        if len(res) < 1:
            raise APIError('Not Found', 404)
        revision = res[0][0]
        return jsonify(revision)

    @app.route('/documents/<title>', methods=['POST'])
    def create_revision(title):
        # Create a new revision of a document

        try:
            body = request.get_json(force=True)
        except:
            raise APIError('No valid JSON body', 422)

        if 'content' not in body:
            raise APIError('Content is missing', 422)

        content = body['content']
        db = get_db()

        res = db.execute("""
            SELECT id
            FROM document
            WHERE title = '%s'
        """ % title).fetchall()

        if len(res) < 1:
            db.execute("""
                INSERT INTO document (title)
                VALUES ('%s')
            """ % title)

        db.execute("""
            INSERT INTO revision (document_id, content)
            SELECT id, '%s'
            FROM document
            WHERE title = '%s'
        """ % (content, title))

        db.commit()
        return jsonify({'message': 'OK'})

    return app
