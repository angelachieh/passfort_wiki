-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS document;
DROP TABLE IF EXISTS revision;

CREATE TABLE document (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title VARCHAR(50) UNIQUE NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE revision (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  document_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  content TEXT NOT NULL,
  FOREIGN KEY (document_id) REFERENCES document (id)
);
